variable "region" {
  default     = "us-east-2"
  description = "AWS region"
  type        = string
}

variable "cluster_name" {
  default     = "gitlab-terraform-eks"
  description = "EKS Cluster name"
  type        = string
}

variable "cluster_version" {
  default     = "1.31"
  description = "Kubernetes version"
  type        = string
}

variable "instance_type" {
  default     = "t3.small"
  description = "EKS node instance type"
  type        = string
}

variable "instance_count" {
  default     = 3
  description = "EKS node count"
  type        = number
}

variable "agent_namespace" {
  default     = "gitlab-agent"
  description = "Kubernetes namespace to install the Agent"
  type        = string
}

variable "agent_token" {
  description = "Agent token (provided after registering an Agent in GitLab)"
  sensitive   = true
  type        = string
}

variable "kas_address" {
  description = "Agent Server address (provided after registering an Agent in GitLab)"
  type        = string
}
